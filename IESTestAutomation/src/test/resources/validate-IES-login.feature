# language: en

@IESLoginValidate
Feature: Validate IES System Login
  As a user
  I would like to login to the IES System
	So that I can access the system
  
  Background: 
  	Given that the user has access to the IES system login page
  						
  @IESLoginSuccessValidate
  Scenario Outline: IES system login successfully
    When the user enters with your "<email>" and "<password>"
    Then the user will be successfully logged in
    
    Examples:
    |         email         |    password    |
    |  alebsiufu@gmail.com  |   Testies123   |    
    
  @IESLoginEmptyValidate
  Scenario Outline: IES System Login with empty username and/or password
    When the user enters with your "<email>" and or "<password>" with empty values
    Then the user will not be successfully logged in because fields "<email>" and "<password>" are required
    
    Examples:
    |        email        |    password    |
    |                     |   Testies123   |
    | alebsiufu@gmail.com |                |
    |                     |                |
    
  @IESLoginInvalidCredentialsValidate
  Scenario Outline: IES System Login with invalid username and/or password
    When the user enters with your "<email>" and or "<password>" with invalid values
    Then the user will not be successfully logged in because the credentials are invalid
    
    Examples:
    |        email        |    password    |
    |   test@gmail.com    |   Testies123   |
    | alebsiufu@gmail.com |   Test123456   |
    |   test@gmail.com    |   Test123456   |