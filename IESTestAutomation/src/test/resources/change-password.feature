# language: en

@validateChangePassword
Feature: Validate Change Password Account
  As a user
  I would like to access the IES system with my account
  So that I can change my password
  	
  @validateChangePasswordSuccess
  Scenario Outline: Change Password Account
  	Given that the user has access to your account on IES system with your "<email>" and "<password>"
    When the user change your "<password>" account
    Then the user will have your password changed with success
    
     Examples:
    |         email         |    password    |
    |  alebsiufu@gmail.com  |   Testies123   |  
    
   