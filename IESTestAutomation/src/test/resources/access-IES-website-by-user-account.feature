# language: en

@validateAccessIESWebSiteByUserAccount
Feature: Validate Access IES Website By User Account
  As a user
  I would like to access the IES system 
  So that I can access the IES WebSite by my account
  	
  @validateIESWebSiteByUserAccountSuccess
  Scenario Outline: Access IES Website by my account With Success
  	Given that the user has access to the IES system with your "<email>" and "<password>"
    When the user clicks on your account's website menu
    Then the user is redirected to IES Website with success by your account
    
     Examples:
    |         email         |    password    |
    |  alebsiufu@gmail.com  |   Testies123   |  

    
   