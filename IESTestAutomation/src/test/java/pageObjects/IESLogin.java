package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class IESLogin {
	
private static WebElement element = null;
	
	public static WebElement emailField(WebDriver driver){
		
	    element = driver.findElement(By.id("email"));
	    return element;
	    
	}
	
	public static WebElement passwordField(WebDriver driver){
		
	    element = driver.findElement(By.id("password"));
	    return element;
	    
	}

	public static WebElement loginButton(WebDriver driver){
		
	    element = driver.findElement(By.id("login"));
	    return element;
	    
	}
	
	public static WebElement alertSuccessMessage(WebDriver driver){
		
	    element = driver.findElement(By.cssSelector(".alert-success li"));
	    return element;
	    
	}
	
	public static WebElement messageHeader(WebDriver driver){
		
	    element = driver.findElement(By.xpath("/html/body/div/div[2]/div[2]/p[1][contains(text(), 'Hi Alessandra')]"));
	    return element;
	    
	}
	
	public static WebElement welcomeMessage(WebDriver driver){
		
	    element = driver.findElement(By.xpath("/html/body/div/div[2]/div[2]/p[2][contains(text(), 'Welcome to your IES account page. In this section you can update your personal details, access your latest purchases and downloads, manage your training bookings and find out more about our latest products and services.')]"));
	    return element;
	    
	}
	
	public static WebElement nameAccount(WebDriver driver){
		
	    element = driver.findElement(By.cssSelector(".admin-header a[href='/my-account/profile']"));
	    return element;
	    
	}
	
	public static WebElement requiredMessageOnEmailField(WebDriver driver){
	    
		element = driver.findElement(By.cssSelector("#email-element .errors li"));
	    return element;
	    
	}
	
	public static WebElement requiredMessageOnPasswordField(WebDriver driver){
	   
		element = driver.findElement(By.cssSelector("#password-element .errors li"));
	    return element;
	    
	}
	
	public static WebElement alertDanger(WebDriver driver){
		   
		element = driver.findElement(By.className("alert-danger"));
	    return element;
	    
	}

}
