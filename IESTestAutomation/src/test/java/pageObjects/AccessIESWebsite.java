package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AccessIESWebsite {
	
	private static WebElement element = null;
	
	public static WebElement iesWebsiteMenu(WebDriver driver){
		
	    element = driver.findElement(By.xpath("/html/body/div/div[1]/ul[3]/li[1]/a[contains(text(), 'IES Website')]"));
	    return element;

	}
	
	public static WebElement logOutIESHomePage(WebDriver driver){
		
	    element = driver.findElement(By.cssSelector("#credentials a[href='/logout']"));
	    return element;

	}
	
	public static WebElement iesWebsiteTextElement(WebDriver driver){
		
	    element = driver.findElement(By.cssSelector("#icl-home-hero .text-content h1"));
	    return element;

	}

}
