package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ChangePassword {
	
private static WebElement element = null;
	
	public static WebElement changePasswordMenu(WebDriver driver){
		
	    element = driver.findElement(By.cssSelector(".change-password a[href='/my-account/change-password']"));
	    return element;
	    
	}
	
	public static WebElement currentPasswordField(WebDriver driver){
		
	    element = driver.findElement(By.id("currentpassword"));
	    return element;
	    
	}
	
	public static WebElement newPasswordField(WebDriver driver){
		
	    element = driver.findElement(By.id("password1"));
	    return element;
	    
	}
	
	public static WebElement confirmNewPasswordField(WebDriver driver){
		
	    element = driver.findElement(By.id("password2"));
	    return element;
	    
	}

	public static WebElement changePasswordButton(WebDriver driver){
		
	    element = driver.findElement(By.id("submit_button"));
	    return element;
	    
	}
	
	public static WebElement changePasswordMessageSuccess(WebDriver driver){
		
	    element = driver.findElement(By.cssSelector(".alert-success li"));
	    return element;
	    
	}
	
}
