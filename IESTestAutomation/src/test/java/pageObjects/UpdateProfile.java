package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class UpdateProfile {
	
private static WebElement element = null;
	
	public static WebElement profileMenu(WebDriver driver){
		
	    element = driver.findElement(By.xpath("/html/body/div/div[1]/ul[1]/li[2]/a[contains(text(), 'Profile')]"));
	    return element;
	    
	}
	
	public static WebElement firstNameField(WebDriver driver){
		
	    element = driver.findElement(By.id("firstname"));
	    return element;
	    
	}
	
	public static WebElement lastNameField(WebDriver driver){
		
	    element = driver.findElement(By.id("lastname"));
	    return element;
	    
	}

	public static WebElement submitButton(WebDriver driver){
		
	    element = driver.findElement(By.id("profile_submit"));
	    return element;
	    
	}
	
	public static WebElement nameHeader(WebDriver driver){
		
	    element = driver.findElement(By.cssSelector(".admin-header a[href='/my-account/profile']"));
	    return element;
	    
	}
	
}
