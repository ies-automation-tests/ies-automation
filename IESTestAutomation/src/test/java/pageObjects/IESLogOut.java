package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class IESLogOut {

private static WebElement element = null;
	
	public static WebElement signOutHeader(WebDriver driver){
		
	    element = driver.findElement(By.cssSelector(".admin-header a[href='/logout']"));
	    return element;
	    
	}
	
	public static WebElement loginIESHomePage(WebDriver driver){
		
	    element = driver.findElement(By.cssSelector("#credentials a[href='/login']"));
	    return element;
	    
	}
	
}
