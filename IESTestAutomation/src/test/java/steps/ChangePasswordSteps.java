package steps;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.ChangePassword;
import pageObjects.IESLogOut;
import pageObjects.IESLogin;

public class ChangePasswordSteps {
	
	private WebDriver driver;
	private WebDriverWait wait;
	
	@Given("^that the user has access to your account on IES system with your \"([^\"]*)\" and \"([^\"]*)\"$")
	public void thatTheUserHasAccessToYourAccountOnIESSystemWithYourAnd(String email, String password) throws Throwable {
	    
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader("asserts/env.json"));
		JSONObject jsonObject = (JSONObject) object;
		
		String chromeDriver = (String) jsonObject.get("chromeDriver");
		String IESLoginPage = (String) jsonObject.get("IESLoginPage");
		
		System.setProperty("webdriver.chrome.driver", chromeDriver);
		driver = new ChromeDriver();
		driver.get(IESLoginPage);
		driver.manage().window().maximize();
		wait = new WebDriverWait(driver, 3);
		
		IESLogin.emailField(driver).click();
		IESLogin.emailField(driver).sendKeys(email);
		IESLogin.passwordField(driver).click();
		IESLogin.passwordField(driver).sendKeys(password);
		IESLogin.loginButton(driver).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".change-password a[href='/my-account/change-password']")));
		
	}

	@When("^the user change your \"([^\"]*)\" account$")
	public void theUserChangeYourAccount(String password) throws Throwable {
		
		ChangePassword.changePasswordMenu(driver).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.id("currentpassword")));
		
		ChangePassword.currentPasswordField(driver).click();
		ChangePassword.currentPasswordField(driver).sendKeys(password);
		ChangePassword.newPasswordField(driver).click();
		ChangePassword.newPasswordField(driver).sendKeys("Test123456");
		ChangePassword.confirmNewPasswordField(driver).click();
		ChangePassword.confirmNewPasswordField(driver).sendKeys("Test123456");
		ChangePassword.changePasswordButton(driver).click();
		
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader("asserts/values.json"));
		JSONObject jsonObject = (JSONObject) object;
		
		String changePasswordMessageJson = (String) jsonObject.get("changePasswordMessage");
		String changePasswordMessage = ChangePassword.changePasswordMessageSuccess(driver).getText();
		Assert.assertEquals(changePasswordMessageJson, changePasswordMessage);
		System.out.println(changePasswordMessageJson + " json change password message");
		System.out.println(changePasswordMessage + " url change password message on the page ");
		
		IESLogOut.signOutHeader(driver).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#credentials a[href='/login']")));
		
		IESLogOut.loginIESHomePage(driver).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.id("email")));
		
		IESLogin.emailField(driver).click();
		IESLogin.emailField(driver).sendKeys("alebsiufu@gmail.com");
		IESLogin.passwordField(driver).click();
		IESLogin.passwordField(driver).sendKeys("Test123456");
		IESLogin.loginButton(driver).click();
		
	}

	@Then("^the user will have your password changed with success$")
	public void theUserWillHaveYourPasswordChangedWithSuccess() throws Throwable {
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".alert-success li")));
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".change-password a[href='/my-account/change-password']")));
		
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader("asserts/values.json"));
		JSONObject jsonObject = (JSONObject) object;
		
		String currentUrlJson = (String) jsonObject.get("currentUrl");
		String currentUrlPage = driver.getCurrentUrl();
		Assert.assertEquals(currentUrlJson, currentUrlPage);
		System.out.println(currentUrlJson + " json url ");
		System.out.println(currentUrlPage + " url on the page ");
		
		String alertSuccessMessageJson = (String) jsonObject.get("alertSuccessMessage");
		String alertSuccessMessage = IESLogin.alertSuccessMessage(driver).getText();
		Assert.assertEquals(alertSuccessMessageJson, alertSuccessMessage);
		System.out.println(alertSuccessMessageJson + " json alert success message ");
		System.out.println(alertSuccessMessage + " alert success message on the page ");
		
		ChangePassword.changePasswordMenu(driver).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.id("currentpassword")));
		
		ChangePassword.currentPasswordField(driver).click();
		ChangePassword.currentPasswordField(driver).sendKeys("Test123456");
		ChangePassword.newPasswordField(driver).click();
		ChangePassword.newPasswordField(driver).sendKeys("Testies123");
		ChangePassword.confirmNewPasswordField(driver).click();
		ChangePassword.confirmNewPasswordField(driver).sendKeys("Testies123");
		ChangePassword.changePasswordButton(driver).click();
		
		String changePasswordMessageJson = (String) jsonObject.get("changePasswordMessage");
		String changePasswordMessage = ChangePassword.changePasswordMessageSuccess(driver).getText();
		Assert.assertEquals(changePasswordMessageJson, changePasswordMessage);
		System.out.println(changePasswordMessageJson + " json change password message");
		System.out.println(changePasswordMessage + " url change password message on the page ");
		
	}
	
	@After("@validateChangePassword")
	public void screenshotConfiguracao(Scenario cenario) {
		
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		
		try {
			
			FileUtils.copyFile(file, new File("target/screenshots/change-password/" + cenario.getId() + ".jpg"));
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		}
		
		driver.quit();
		
		System.out.println("End");
		
	}

}
