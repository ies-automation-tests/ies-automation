package steps;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.IESLogin;

public class ValidateIESLoginSteps {
	
	private WebDriver driver;
	private WebDriverWait wait;
	
	@Given("^that the user has access to the IES system login page$")
	public void thatTheUserHasAccessToTheIESSystemLoginPage() throws Throwable {
		
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader("asserts/env.json"));
		JSONObject jsonObject = (JSONObject) object;
		
		String chromeDriver = (String) jsonObject.get("chromeDriver");
		String IESLoginPage = (String) jsonObject.get("IESLoginPage");
		
		System.setProperty("webdriver.chrome.driver", chromeDriver);
		driver = new ChromeDriver();
		driver.get(IESLoginPage);
		driver.manage().window().maximize();
		wait = new WebDriverWait(driver, 3);
	  
	}

	@When("^the user enters with your \"([^\"]*)\" and \"([^\"]*)\"$")
	public void theUserEntersWithYourAnd(String email, String password) throws Throwable {
		
		IESLogin.emailField(driver).click();
		IESLogin.emailField(driver).sendKeys(email);
		IESLogin.passwordField(driver).click();
		IESLogin.passwordField(driver).sendKeys(password);
		IESLogin.loginButton(driver).click();
	   
	}

	@Then("^the user will be successfully logged in$")
	public void theUserWillBeSuccessfullyLoggedIn() throws Throwable {
		
		String currentUrlPage = null;
		String alertSuccessMessage = "", messageHeader = "", welcomeMessage = "", nameAccount= "";
		
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader("asserts/values.json"));
		JSONObject jsonObject = (JSONObject) object;
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".alert-success li")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div/div[2]/div[2]/p[1]")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div/div[2]/div[2]/p[2]")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".admin-header a[href='/my-account/profile']")));
		
		String currentUrlJson = (String) jsonObject.get("currentUrl");
		currentUrlPage = driver.getCurrentUrl();
		Assert.assertEquals(currentUrlJson, currentUrlPage);
		System.out.println(currentUrlJson + " json url ");
		System.out.println(currentUrlPage + " url on the page ");
		
		String alertSuccessMessageJson = (String) jsonObject.get("alertSuccessMessage");
		alertSuccessMessage = IESLogin.alertSuccessMessage(driver).getText();
		Assert.assertEquals(alertSuccessMessageJson, alertSuccessMessage);
		System.out.println(alertSuccessMessageJson + " json alert success message ");
		System.out.println(alertSuccessMessage + " alert success message on the page ");
		
		String messageHeaderJson = (String) jsonObject.get("messageHeader");
		messageHeader = IESLogin.messageHeader(driver).getText();
		Assert.assertEquals(messageHeaderJson, messageHeader);
		System.out.println(messageHeaderJson + " json message header ");
		System.out.println(messageHeader + " message header on the page ");
		
		String welcomeMessageJson = (String) jsonObject.get("welcomeMessage");
		welcomeMessage = IESLogin.welcomeMessage(driver).getText();
		Assert.assertEquals(welcomeMessageJson, welcomeMessage);
		System.out.println(welcomeMessageJson + " json welcome message ");
		System.out.println(welcomeMessage + " welcome message on the page ");
		
		String nameAccountJson = (String) jsonObject.get("nameAccount");
		nameAccount = IESLogin.nameAccount(driver).getText();
		Assert.assertEquals(nameAccountJson, nameAccount);
		System.out.println(nameAccountJson + " json name account ");
		System.out.println(nameAccount + " name account on the page ");

	}

	@When("^the user enters with your \"([^\"]*)\" and or \"([^\"]*)\" with empty values$")
	public void theUserEntersWithYourAndOrWithEmptyValues(String email, String password) throws Throwable {
		
		IESLogin.emailField(driver).click();
		IESLogin.emailField(driver).sendKeys(email);
		IESLogin.passwordField(driver).click();
		IESLogin.passwordField(driver).sendKeys(password);
		IESLogin.loginButton(driver).click();
		
		if ((email.isEmpty() || email == null) && (password.isEmpty() || password == null)) {
			
			wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#email-element .errors li")));
			wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#password-element .errors li")));
		
		} else if (email.isEmpty() || email == null) {
			
			wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#email-element .errors li")));
			
		} else {
			
			wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#password-element .errors li")));
			
		}
	  
	}

	@Then("^the user will not be successfully logged in because fields \"([^\"]*)\" and \"([^\"]*)\" are required$")
	public void theUserWillNotBeSuccessfullyLoggedInBecauseFieldsAndAreRequired(String email, String password) throws Throwable {
		
		String requiredMessageOnEmailField = "", requiredMessageOnPasswordField = "";
		
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader("asserts/values.json"));
		JSONObject jsonObject = (JSONObject) object;
		
		if ((email.isEmpty() || email == null) && (password.isEmpty() || password == null)) {
			
			String requiredMessageOnEmailFieldJson = (String) jsonObject.get("requiredMessageEmptyField");
			requiredMessageOnEmailField = IESLogin.requiredMessageOnEmailField(driver).getText();
			Assert.assertEquals(requiredMessageOnEmailFieldJson, requiredMessageOnEmailField);
			System.out.println(requiredMessageOnEmailFieldJson + " required message on email field json, empty email");
			System.out.println(requiredMessageOnEmailField + " required message on email field on the page, empty email");
			
			String requiredMessageOnPasswordFieldJson = (String) jsonObject.get("requiredMessageEmptyField");
			requiredMessageOnPasswordField = IESLogin.requiredMessageOnPasswordField(driver).getText();
			Assert.assertEquals(requiredMessageOnPasswordFieldJson, requiredMessageOnPasswordField);
			System.out.println(requiredMessageOnPasswordFieldJson + " required message on password field json, empty password");
			System.out.println(requiredMessageOnPasswordField + " required message on password field on the page, empty password");
			
		} else if (email.isEmpty() || email == null) {
			
			String requiredMessageOnEmailFieldJson = (String) jsonObject.get("requiredMessageEmptyField");
			requiredMessageOnEmailField = IESLogin.requiredMessageOnEmailField(driver).getText();
			Assert.assertEquals(requiredMessageOnEmailFieldJson, requiredMessageOnEmailField);
			System.out.println(requiredMessageOnEmailFieldJson + " required message on email field json, empty email");
			System.out.println(requiredMessageOnEmailField + " required message on email field on the page, empty email");
		
		} else {
			
			String requiredMessageOnPasswordFieldJson = (String) jsonObject.get("requiredMessageEmptyField");
			requiredMessageOnPasswordField = IESLogin.requiredMessageOnPasswordField(driver).getText();
			Assert.assertEquals(requiredMessageOnPasswordFieldJson, requiredMessageOnPasswordField);
			System.out.println(requiredMessageOnPasswordFieldJson + " required message on password field json, empty password");
			System.out.println(requiredMessageOnPasswordField + " required message on password field on the page, empty password");
			
		}
	    
	}

	@When("^the user enters with your \"([^\"]*)\" and or \"([^\"]*)\" with invalid values$")
	public void theUserEntersWithYourAndOrWithInvalidValues(String email, String password) throws Throwable {
		
		IESLogin.emailField(driver).click();
		IESLogin.emailField(driver).sendKeys(email);
		IESLogin.passwordField(driver).click();
		IESLogin.passwordField(driver).sendKeys(password);
		IESLogin.loginButton(driver).click();
	    
		wait.until(ExpectedConditions.presenceOfElementLocated(By.className("alert-danger")));
		
	}

	@Then("^the user will not be successfully logged in because the credentials are invalid$")
	public void theUserWillNotBeSuccessfullyLoggedInBecauseTheCredentialsAreInvalid() throws Throwable {
		
		String invalidCredentials = "";
		
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader("asserts/values.json"));
		JSONObject jsonObject = (JSONObject) object;
			
		String alertInvalidCredentialsJson = (String) jsonObject.get("alertInvalidCredentials");
		invalidCredentials = IESLogin.alertDanger(driver).getText();
		Assert.assertEquals(alertInvalidCredentialsJson, invalidCredentials);
		System.out.println(alertInvalidCredentialsJson + " alert message json, invalid credentials");
		System.out.println(invalidCredentials + " alert message on the page, invalid credentials");
	
	}

	@After("@IESLoginValidate")
	public void screenshotConfiguracao(Scenario cenario) {
		
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		
		try {
			
			FileUtils.copyFile(file, new File("target/screenshots/validate-login/" + cenario.getId() + ".jpg"));
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		}
		
		driver.quit();
		
		System.out.println("End");
		
	}
	
}
