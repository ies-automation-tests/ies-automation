package steps;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.IESLogin;
import pageObjects.IESLogOut;

public class ValidateIESLogOutSteps {
	
	private WebDriver driver;
	private WebDriverWait wait;
	
	@Given("^that the user has access to the IES system$")
	public void thatTheUserHasAccessToTheIESSystem() throws Throwable {
		
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader("asserts/env.json"));
		JSONObject jsonObject = (JSONObject) object;
		
		String chromeDriver = (String) jsonObject.get("chromeDriver");
		String IESLoginPage = (String) jsonObject.get("IESLoginPage");
		
		System.setProperty("webdriver.chrome.driver", chromeDriver);
		driver = new ChromeDriver();
		driver.get(IESLoginPage);
		driver.manage().window().maximize();
		wait = new WebDriverWait(driver, 3);
		
	}
	
	@When("^the user access the IES system with your \"([^\"]*)\" and \"([^\"]*)\"$")
	public void theUserAccessTheIESSystemWithYourAnd(String email, String password) throws Throwable {
		
		IESLogin.emailField(driver).click();
		IESLogin.emailField(driver).sendKeys(email);
		IESLogin.passwordField(driver).click();
		IESLogin.passwordField(driver).sendKeys(password);
		IESLogin.loginButton(driver).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".admin-header a[href='/logout']")));
		
	}

	@And("^the user log out of the system by header button$")
	public void theUserLogOutOfTheSystemByHeaderButton() throws Throwable {
		
		IESLogOut.signOutHeader(driver).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#credentials a[href='/login']")));
		
	}

	@Then("^the user log out of the system by header button with success$")
	public void theUserLogOutOfTheSystemByHeaderButtonWithSuccess() throws Throwable {
		
		String currentUrlPage = null;
		
		if (IESLogOut.loginIESHomePage(driver).isEnabled()) {
			
			JSONParser parser = new JSONParser();
			Object object = parser.parse(new FileReader("asserts/values.json"));
			JSONObject jsonObject = (JSONObject) object;
			
			String currentUrlJson = (String) jsonObject.get("IESUrl");
			currentUrlPage = driver.getCurrentUrl();
			Assert.assertEquals(currentUrlJson, currentUrlPage);
			System.out.println(currentUrlJson + " json url ");
			System.out.println(currentUrlPage + " url on the page ");
			
		} else {
			
			System.out.println("Error. User has not been logged out of the system");
			
			IESLogOut.signOutHeader(driver).isEnabled();
			
		}
		
	}

	@After("@IESLogOutValidate")
	public void screenshotConfiguracao(Scenario cenario) {
		
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		
		try {
			
			FileUtils.copyFile(file, new File("target/screenshots/validate-logout/" + cenario.getId() + ".jpg"));
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		}
		
		driver.quit();
		
		System.out.println("End");
		
	}

}
