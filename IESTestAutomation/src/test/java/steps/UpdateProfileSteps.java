package steps;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.IESLogin;
import pageObjects.UpdateProfile;

public class UpdateProfileSteps {
	
	private WebDriver driver;
	private WebDriverWait wait;
	
	@Given("^that the user has access to IES system with your \"([^\"]*)\" and \"([^\"]*)\" account$")
	public void thatTheUserHasAccessToIESSystemWithYourAndAccount(String email, String password) throws Throwable {
		
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader("asserts/env.json"));
		JSONObject jsonObject = (JSONObject) object;
		
		String chromeDriver = (String) jsonObject.get("chromeDriver");
		String IESLoginPage = (String) jsonObject.get("IESLoginPage");
		
		System.setProperty("webdriver.chrome.driver", chromeDriver);
		driver = new ChromeDriver();
		driver.get(IESLoginPage);
		driver.manage().window().maximize();
		wait = new WebDriverWait(driver, 3);
		
		IESLogin.emailField(driver).click();
		IESLogin.emailField(driver).sendKeys(email);
		IESLogin.passwordField(driver).click();
		IESLogin.passwordField(driver).sendKeys(password);
		IESLogin.loginButton(driver).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div/div[1]/ul[1]/li[2]/a[contains(text(), 'Profile')]")));
	    
	}

	@When("^the user update your profile$")
	public void theUserUpdateYourProfile() throws Throwable {
		
		UpdateProfile.profileMenu(driver).click();
		UpdateProfile.firstNameField(driver).click();
		UpdateProfile.firstNameField(driver).clear();
		UpdateProfile.firstNameField(driver).sendKeys("Alessandra Santos");
		UpdateProfile.lastNameField(driver).click();
		UpdateProfile.lastNameField(driver).clear();
		UpdateProfile.lastNameField(driver).sendKeys("Costa Silva");
		UpdateProfile.submitButton(driver).click();
	  
	}

	@Then("^the user will have your profile updated with success$")
	public void theUserWillHaveYourProfileUpdatedWithSuccess() throws Throwable {
		
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader("asserts/values.json"));
		JSONObject jsonObject = (JSONObject) object;
		
		String updateProfileJson = (String) jsonObject.get("updateProfile");
		String updateProfile = UpdateProfile.nameHeader(driver).getText();
		Assert.assertEquals(updateProfileJson, updateProfile);
		System.out.println(updateProfileJson + " json update profile name");
		System.out.println(updateProfile + " url update profile name on the page ");
		
		UpdateProfile.profileMenu(driver).click();
		UpdateProfile.firstNameField(driver).click();
		UpdateProfile.firstNameField(driver).clear();
		UpdateProfile.firstNameField(driver).sendKeys("Alessandra");
		UpdateProfile.lastNameField(driver).click();
		UpdateProfile.lastNameField(driver).clear();
		UpdateProfile.lastNameField(driver).sendKeys("Costa");
		UpdateProfile.submitButton(driver).click();

	}

	@After("@validateUpdateProfile")
	public void screenshotConfiguracao(Scenario cenario) {
		
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		
		try {
			
			FileUtils.copyFile(file, new File("target/screenshots/update-profile/" + cenario.getId() + ".jpg"));
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		}
		
		driver.quit();
		
		System.out.println("End");
		
	}

}
