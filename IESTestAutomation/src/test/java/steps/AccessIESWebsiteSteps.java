package steps;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.AccessIESWebsite;
import pageObjects.IESLogin;

public class AccessIESWebsiteSteps {
	
	private WebDriver driver;
	private WebDriverWait wait;
	
	@Given("^that the user has access to the IES system with your \"([^\"]*)\" and \"([^\"]*)\"$")
	public void thatTheUserHasAccessToTheIESSystemWithYourAnd(String email, String password) throws Throwable {
		
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader("asserts/env.json"));
		JSONObject jsonObject = (JSONObject) object;
		
		String chromeDriver = (String) jsonObject.get("chromeDriver");
		String IESLoginPage = (String) jsonObject.get("IESLoginPage");
		
		System.setProperty("webdriver.chrome.driver", chromeDriver);
		driver = new ChromeDriver();
		driver.get(IESLoginPage);
		driver.manage().window().maximize();
		wait = new WebDriverWait(driver, 3);
		
		IESLogin.emailField(driver).click();
		IESLogin.emailField(driver).sendKeys(email);
		IESLogin.passwordField(driver).click();
		IESLogin.passwordField(driver).sendKeys(password);
		IESLogin.loginButton(driver).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div/div[1]/ul[3]/li[1]/a[contains(text(), 'IES Website')]")));
	   
	}

	@When("^the user clicks on your account's website menu$")
	public void theUserClicksOnYourAccountSWebsiteMenu() throws Throwable {
		
		AccessIESWebsite.iesWebsiteMenu(driver).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#credentials a[href='/logout']")));
	   
	}

	@Then("^the user is redirected to IES Website with success by your account$")
	public void theUserIsRedirectedToIESWebsiteWithSuccessByYourAccount() throws Throwable {
		
		if(AccessIESWebsite.logOutIESHomePage(driver).isEnabled()) {
		
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#icl-home-hero .text-content h1")));
		
		System.out.println(AccessIESWebsite.iesWebsiteTextElement(driver).getText());
		
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader("asserts/values.json"));
		JSONObject jsonObject = (JSONObject) object;
		
		String textContentIESWebsiteJson = (String) jsonObject.get("textContentIESWebsite");
		String textContentIESWebsite = AccessIESWebsite.iesWebsiteTextElement(driver).getText();
		Assert.assertEquals(textContentIESWebsiteJson, textContentIESWebsite);
		System.out.println(textContentIESWebsiteJson + " json text content ");
		System.out.println(textContentIESWebsite + " text content on the page ");
		
		String currentUrlJson = (String) jsonObject.get("IESUrl");
		String currentUrlPage = driver.getCurrentUrl();
		Assert.assertEquals(currentUrlJson, currentUrlPage);
		System.out.println(currentUrlJson + " json url ");
		System.out.println(currentUrlPage + " url on the page ");
		
		} else {
			
			System.out.println("Error. User did not access IES website");
			
			AccessIESWebsite.iesWebsiteMenu(driver).isEnabled();
			
		}
	   
	}
	
	@After("@validateAccessIESWebSiteByUserAccount")
	public void screenshotConfiguracao(Scenario cenario) {
		
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		
		try {
			
			FileUtils.copyFile(file, new File("target/screenshots/validate-access-website/" + cenario.getId() + ".jpg"));
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		}
		
		driver.quit();
		
		System.out.println("End");
		
	}

}
