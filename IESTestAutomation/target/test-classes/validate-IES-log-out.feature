# language: en

@IESLogOutValidate
Feature: Validate Log Out IES System 
  As a user
  I would like to log out the IES system 
  So that I can access the login system page
  
  Background:
  	Given that the user has access to the IES system
  						
  @IESButtonLogOutHeaderSuccessValidate
  Scenario Outline: Header button Log out IES System With Success
  	When the user access the IES system with your "<email>" and "<password>"
    And the user log out of the system by header button
    Then the user log out of the system by header button with success
    
     Examples:
    |         email         |    password    |
    |  alebsiufu@gmail.com  |   Testies123   |  
    
   