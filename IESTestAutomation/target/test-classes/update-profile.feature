# language: en

@validateUpdateProfile
Feature: Validate Update Profile Account
  As a user
  I would like to access the IES system with my account
  So that I can update my profile
  	
  @validateUpdateProfileSuccess
  Scenario Outline: Update Profile With Success
  	Given that the user has access to IES system with your "<email>" and "<password>" account
    When the user update your profile
    Then the user will have your profile updated with success
    
     Examples:
    |         email         |    password    |
    |  alebsiufu@gmail.com  |   Testies123   |  